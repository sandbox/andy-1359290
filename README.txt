Link select
***********

Description
***********

This module provides a new widget for the (CCK) Link module[1]. The widget
comprises a select list, potentially split up into different option groups. The
groups can be menus, node types, or both (in which case only those nodes that
aren't included in the menus will be shown).

[1] http://drupal.org/project/link
